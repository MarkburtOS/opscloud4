package com.baiyi.opscloud.order.enums;

/**
 * 工单审批动作
 * @Author baiyi
 * @Date 2021/11/11 11:23 上午
 * @Version 1.0
 */
public enum ApprovalTypeEnum {

    AGREE,  //同意
    CANCEL, //取消
    REJECT  //拒绝
}
